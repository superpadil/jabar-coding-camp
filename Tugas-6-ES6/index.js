// Soal 1
console.log("Jawaban soal 1")
let LK = (P, L) => {
    console.log(`Luas  : ${P * L}`);
    console.log(`Keliling  : ${(P + L)*2}`);
}

console.log('Jika panjang = 5 dan lebar = 3 maka :' )+LK(5, 3);
console.log('Jika panjang = 10 dan lebar = 13 maka :')+LK(10, 13);

//soal 2
console.log("Jawaban soal 2")
const Fullname = (firstName, lastName) => {
    return {
        func : () => {
            console.log(`${firstName} ${lastName}`);
        }
    }
}

Fullname("William", "Imoh").func() 

//soal 3
console.log("Jawaban soal 3")

const newObject = {
	firstName: "Muhammad",
	lastName: "Iqbal Mubarok",
	address: "Jalan Ranamanyar",
	hobby: "playing football",
};

const { firstName, lastName, address, hobby } = newObject;
console.log(firstName, lastName, address, hobby);

//soal4
console.log("Jawaban soal 4")

const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]

const combined = [... west, ... east];
console.log(combined)

//soal5
console.log("Jawaban soal 5")
const planet = "earth" 
const view = "glass" 

const after = `Lorem ${view} dolor sit amet, consectetur adipiscing elit, ${planet}`;
console.log(after);



