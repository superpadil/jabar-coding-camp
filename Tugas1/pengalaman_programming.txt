Saya tidak banyak pengalaman programming dalam hal yang bertujuan untuk
memanipulasi sebuah perangkat lunak, web, atau sejenisnya
Namun saya memiliki pengalaman programming pada perangkat lunak 
yang memungkinkan untuk mengolah data yang banyak seperti MatLab dan Octave
Sehingga hal tersebut dapat membantu saya untuk tidak terlalu gagap 
dalam memulai perjalanan yang baru dalam dunia programming di Front-end Web ini. 
