var readBooksPromise = require('./promise.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000},
    {name: 'komik', timeSpent: 1000}
]
 
// Lanjutkan code untuk menjalankan function readBooksPromise 

readBooksPromise(10000,books[0])
    .then(function(timeleft){
        readBooksPromise(timeleft,books[1])
        .then(function(timeleft0){
            readBooksPromise(timeleft0,books[2])
            .then(function(timeleft1){
                readBooksPromise(timeleft1,books[3])
            })
        })
    })
  .catch(error => console.log(error))
